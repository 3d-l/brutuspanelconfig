Brutus One Panel Config
Copyright (C) 2018  Carsten Lehmann, 3nb electronics GbR Daniel Otto & Carsten Lehmann

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Collections.ObjectModel;
using Renci.SshNet;

namespace BrutusPanelConfig
{

    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }



        private void Window_Initialized(object sender, EventArgs e)
        {
            MainWindow1.IsEnabled = false;
            MediaElement1.LoadedBehavior = MediaState.Manual;
            DirectoryInfo d = new DirectoryInfo(@"C:\Animations");//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.mp4"); //Getting Text files

            Int16 n = 0;
            foreach (FileInfo file in Files)
            {
                n++;
                Button ThumbButton = new Button();
                System.Windows.Controls.Image BGImg = new System.Windows.Controls.Image
                {
                    Source = (BitmapToImageSource(GetThumbnail(file.Name, "C:\\Animations\\Thumb.jpg")))
                };
                ThumbButton.Name = "Button" + n.ToString();
                ThumbButton.Width = 100;
                ThumbButton.Height = double.NaN;
                ThumbButton.Content = BGImg;
                ThumbButton.Tag = file.Name;
                ThumbButton.Margin = new Thickness(10);
                ThumbButton.Click += new RoutedEventHandler(ButtonClickOneEvent);
                StackPanel1.Children.Add(ThumbButton);
                File.Delete("C://Animations//Thumb.jpg");
            }


            MainWindow1.IsEnabled = true;
        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            MainWindow1.IsEnabled = false;

            using (var client = new SshClient("192.168.99.99", "pi", "123456"))
            {
                try
                {
                    client.Connect();
                    client.RunCommand("sudo pkill omxplayer");
                    Task.Delay(500);
                    client.Dispose();
                }
                catch (Exception)
                {
                    System.Windows.MessageBox.Show("Fehler beim stoppen der Animation");
                }

            }

            using (WebClient client = new WebClient())
            {
                try
                {
                    client.UploadProgressChanged += new UploadProgressChangedEventHandler(UpProChange);
                    client.UploadFileCompleted += new UploadFileCompletedEventHandler(UpEnd);
                    client.Credentials = new NetworkCredential("pi", "123456");
                    client.UploadFileAsync(new Uri("ftp://192.168.99.99/Animations/1.mp4", UriKind.RelativeOrAbsolute), "STOR", "C://Animations//" + ButtonApply.Tag.ToString());
                }
                catch (Exception e1)
                {
                    System.Windows.MessageBox.Show("Fehler bei der Übertragung:  " + e1.Message);
                }

            }
            
        }


        void ButtonClickOneEvent(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                MediaElement1.Source = new Uri("C://Animations//" + button.Tag.ToString(), UriKind.RelativeOrAbsolute);
                MediaElement1.Play();
                ButtonApply.Tag = button.Tag;
            }
        }

        void UpProChange(object sender, UploadProgressChangedEventArgs e)
        {
            ProgressBar1.Value = e.ProgressPercentage;
        }

        void UpEnd(object sender, UploadFileCompletedEventArgs e)
        {
            ProgressBar1.Value = 0;

            using (var client = new SshClient("192.168.99.99", "pi", "123456"))
            {
                try
                {
                    client.Connect();
                    client.RunCommand("sudo nohup omxplayer --no-osd --loop /home/pi/Animations/1.mp4 > /dev/null 2>&1 &");
                    Task.Delay(1500);
                    client.Dispose();
                }
                catch (Exception)
                {
                    System.Windows.MessageBox.Show("Fehler beim starten der Animation");
                }
                MainWindow1.IsEnabled = true;
            }
        }


        public class Field
        {
            public string Name { get; set; }
            public int Length { get; set; }
            public bool Required { get; set; }
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        public static Bitmap GetThumbnail(string video, string thumbnail)
        {
            video = "C:\\Animations\\" + video;
            var cmd = "C:\\ffmpeg\\ffmpeg.exe -itsoffset -1  -i " + '"' + video + '"' + " -vcodec mjpeg -vframes 1 -an -f rawvideo -s 135x169 " + '"' + thumbnail + '"';
                        var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = "cmd.exe",
                Arguments = "/C " + cmd
            };

            var process = new Process
            {
                StartInfo = startInfo
            };

            process.Start();
            process.WaitForExit(5000);

            return LoadImage(thumbnail);
        }

        static Bitmap LoadImage(string path)
        {
            try
            {
                var ms = new MemoryStream(File.ReadAllBytes(path));
                return (Bitmap)System.Drawing.Image.FromStream(ms);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Thumbnail nicht gefunden... ist ffmpeg installiert?");
                return (Bitmap)System.Drawing.Image.FromStream(null);

            }


        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            MainWindow1.IsEnabled = false;
            using (var client = new SshClient("192.168.99.99", "pi", "123456"))
            {
                try
                {
                    client.Connect();
                    client.RunCommand("sudo shutdown -r now");
                    client.Dispose();
                }
                catch (Exception e2)
                {
                    if (e2.HResult != -2146233088) //connection closed is normal when reboot and no error...
                    {
                        System.Windows.MessageBox.Show("Fehler beim Reset: " + e2.Message);
                    }

                }

            }
            MainWindow1.IsEnabled = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }

    }
